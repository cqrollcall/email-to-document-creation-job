package com.cq.email2doc;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cq.udr.DocumentSource;
import com.cq.udr.Pubgroup;


public class Util
{
  private static Log log = LogFactory.getLog(Util.class);
  
  public static final Map<Long, String> sourceToDestinationNameMap;
  
  static {
      Map<Long, String> aMap = new HashMap<Long, String>();
      aMap.put(DocumentSource.CQACTIONALERTS, "cqnow");
      aMap.put(DocumentSource.CQNEWS, "cqnow");
      aMap.put(DocumentSource.CQWEEKLY, "cqnow");
      aMap.put(DocumentSource.CQLIVE, "cqnow");
      aMap.put(DocumentSource.CQSENATE, "sar");
      sourceToDestinationNameMap = Collections.unmodifiableMap(aMap);
  } 

  Map<String,String> entityMap;
  Pattern entityPattern = Pattern.compile( "&(.+?);" );

  public static void delaySecs(int secs)
  {
    try 
    {
      Thread.sleep(secs * 1000);
    } 
    catch(InterruptedException e) {} // ignored
  }
  
  /**
   * Converts unicode chars and numeric entities to their 
   * equivalent named entity
   * 
   * A WORK IN PROGRESS!!
   * 
   * @param text
   * @return
   */
  public static String numericEntitiesToNamedEntities(String text)
  {
    StringBuilder newText = new StringBuilder();

    for (int i = 0, imax = text.length(); i < imax; ++i)
    {
      char ch = text.charAt(i);
      int cp = text.codePointAt(i);
      
      //log.info("ch = " + ch + ", codePointAt(i) = " + cp);

      switch (cp) {
      case 133:
        newText.append("&hellip;");
        break;
      case 145:
        newText.append("&lsquo;");
        break;
      case 146:
        newText.append("&rsquo;");
        break;
      case 147:
        newText.append("&ldquo;");
        break;
      case 148:
        newText.append("&rdquo;");
        break;
      case 150:
        newText.append("&mdash;");
        break;
      case 151:
        newText.append("&mdash;");
        break;
      case 160:
        newText.append("&nbsp;");
        break;        

      case 8230:
        newText.append("&hellip;");
        break;
      case 8216:
        newText.append("&lsquo;");
        break;
      case 8217:
        newText.append("&rsquo;");
        break;
      case 8220:
        newText.append("&ldquo;");
        break;
      case 8221:
        newText.append("&rdquo;");
        break;
      case 8208:
        newText.append("&ndash;");
        break;
      case 8211:
        newText.append("&ndash");
        break;
      case 8212:
        newText.append("&mdash;");
        break;

      default:
        if (cp == 9) // TAB
        {
          newText.append("  ");
        }
        else if ((cp < 32 && cp != 9 && cp != 10 && cp != 13))
        {
          newText.append("[?]");
          log.info(": invalid character found -> " + "[" + (int) cp + "]");
        }
        else
        {
          newText.append(ch);
        }
        break;
      }
    }
    
    return newText.toString();
    
    
  }
  
  /**
   * Converts unicode chars and numeric entities to their 
   * equivalent named entity
   * 
   * A WORK IN PROGRESS!!
   * 
   * @param text
   * @return
   */
  public static String numericEntitiesToPlainText(String text)
  {
    StringBuilder newText = new StringBuilder();

    for (int i = 0, imax = text.length(); i < imax; ++i)
    {
      char ch = text.charAt(i);
      int cp = text.codePointAt(i);
      
      //log.info("ch = " + ch + ", codePointAt(i) = " + cp);

      switch (cp) {
      case 133:
        newText.append("...");
        break;
      case 145:
        newText.append("'");
        break;
      case 146:
        newText.append("'");
        break;
      case 147:
        newText.append("\"");
        break;
      case 148:
        newText.append("\"");
        break;
      case 150:
        newText.append("--");
        break;
      case 151:
        newText.append("--");
        break;
      case 160:
        newText.append(" ");
        break;        

      case 8230:
        newText.append("...");
        break;
      case 8216:
        newText.append("'");
        break;
      case 8217:
        newText.append("'");
        break;
      case 8220:
        newText.append("\"");
        break;
      case 8221:
        newText.append("\"");
        break;
      case 8208:
        newText.append("-");
        break;
      case 8211:
        newText.append("-");
        break;
      case 8212:
        newText.append("--");
        break;

      default:
        if (cp == 9) // TAB
        {
          newText.append("  ");
        }
        else if ((cp < 32 && cp != 9 && cp != 10 && cp != 13))
        {
          newText.append("[?]");
          log.info(": invalid character found -> " + "[" + (int) cp + "]");
        }
        else
        {
          newText.append(ch);
        }
        break;
      }
    }
    
    return newText.toString(); 
  }

  /**
   * Converts named entities to their equivalent unicode chars and numeric entity
   * 
   * A WORK IN PROGRESS!!
   * 
   * @param text
   * @return
   */
  public String namedEntitiesToNumericEntities( String xml )
  {
    StringBuilder sb = new StringBuilder();
    int start = 0;
    Matcher m = entityPattern.matcher( xml );
    while( m.find(start) )
    {
      sb.append( xml.substring( start, m.start() ) );
      
      String decimalValue = entityMap.get( m.group(1) );
      
      if (log.isDebugEnabled())
        log.debug("decimalValue = " + decimalValue);

      if( decimalValue == null )
      {
        sb.append( "&" ).append( m.group(1) ).append( ";" );        
      }
      else
      {
        Integer decimalInt = Integer.parseInt(decimalValue);
        if (decimalInt == 38) {
          sb.append("%%%");
        } else {
          sb.append( "%%%#" ).append( decimalInt ).append( ";" );
        }
      }
      
      start = m.end();
    }
    sb.append( xml.substring( start ));
    
    return sb.toString();
  }
  
  public static boolean specified( String s )
  {
    return s != null && s.trim().length() > 0;
  }
  
  public static boolean specified( Date s )
  {
    return s != null ;
  }
  
  public static String removeEmptyLines( String text )
  {
    if (text == null)
      return null;
    
    StringBuilder sb = new StringBuilder();
    
    for (String line: text.split("\n"))
    {
      if (Util.specified(line))
        sb.append(line).append("\n");
    }
    
    return sb.length() > 0 ? sb.toString() : null;
  }

  
  // Some Microsoft emails have EoM characters in them (decimal 25).  These are not valid Unicode.
  public static String removeNonPrintableCharacters(String txt)
  {
    StringBuilder sb = new StringBuilder();
    
    if (txt == null)
      return null;
    
    for (int i=0; i< txt.length(); i++)
    {
      char c = txt.charAt(i);
      int cVal = (int)c;
      
      if ( cVal >= 32 || (cVal == 9 || cVal == 10 || cVal == 13))
      {
        sb.append( c );
      }
    }
    
    return sb.toString();
  }
  
  public static String removeNonSlugCharacters(String txt)
  {
    StringBuilder sb = new StringBuilder();
    
    if (txt == null)
      return null;
    
    for (int i=0; i< txt.length(); i++)
    {
      char c = txt.charAt(i);
      
      if ( Character.isLetterOrDigit( c )
          || c == '-'
          || c == '_')
      {
        sb.append( c );
      }
    }
    
    return sb.toString();
  }
  
  public static String pluralize(String text, int count)
  {
    if ( count == 1)
      return text;
    else
    {
      if ( !Util.specified( text ) )
        return text;
      return text+"s";
    }
  }

  

  
  public void init()
  {
    entityMap = new HashMap<String,String>();
    entityMap.put("amp","38");
    entityMap.put("apos","39");
    entityMap.put("lt","60");
    entityMap.put("gt","62");
    entityMap.put("nbsp","160");
    entityMap.put("iexcl","161");
    entityMap.put("cent","162");
    entityMap.put("pound","163");
    entityMap.put("curren","164");
    entityMap.put("yen","165");
    entityMap.put("brvbar","166");
    entityMap.put("sect","167");
    entityMap.put("uml","168");
    entityMap.put("copy","169");
    entityMap.put("ordf","170");
    entityMap.put("laquo","171");
    entityMap.put("not","172");
    entityMap.put("shy","173");
    entityMap.put("reg","174");
    entityMap.put("macr","175");
    entityMap.put("deg","176");
    entityMap.put("plusmn","177");
    entityMap.put("sup2","178");
    entityMap.put("sup3","179");
    entityMap.put("acute","180");
    entityMap.put("micro","181");
    entityMap.put("para","182");
    entityMap.put("middot","183");
    entityMap.put("cedil","184");
    entityMap.put("sup1","185");
    entityMap.put("ordm","186");
    entityMap.put("raquo","187");
    entityMap.put("frac14","188");
    entityMap.put("frac12","189");
    entityMap.put("frac34","190");
    entityMap.put("iquest","191");
    entityMap.put("Agrave","192");
    entityMap.put("Aacute","193");
    entityMap.put("Acirc","194");
    entityMap.put("Atilde","195");
    entityMap.put("Auml","196");
    entityMap.put("Aring","197");
    entityMap.put("AElig","198");
    entityMap.put("Ccedil","199");
    entityMap.put("Egrave","200");
    entityMap.put("Eacute","201");
    entityMap.put("Ecirc","202");
    entityMap.put("Euml","203");
    entityMap.put("Igrave","204");
    entityMap.put("Iacute","205");
    entityMap.put("Icirc","206");
    entityMap.put("Iuml","207");
    entityMap.put("ETH","208");
    entityMap.put("Ntilde","209");
    entityMap.put("Ograve","210");
    entityMap.put("Oacute","211");
    entityMap.put("Ocirc","212");
    entityMap.put("Otilde","213");
    entityMap.put("Ouml","214");
    entityMap.put("times","215");
    entityMap.put("Oslash","216");
    entityMap.put("Ugrave","217");
    entityMap.put("Uacute","218");
    entityMap.put("Ucirc","219");
    entityMap.put("Uuml","220");
    entityMap.put("Yacute","221");
    entityMap.put("THORN","222");
    entityMap.put("szlig","223");
    entityMap.put("agrave","224");
    entityMap.put("aacute","225");
    entityMap.put("acirc","226");
    entityMap.put("atilde","227");
    entityMap.put("auml","228");
    entityMap.put("aring","229");
    entityMap.put("aelig","230");
    entityMap.put("ccedil","231");
    entityMap.put("egrave","232");
    entityMap.put("eacute","233");
    entityMap.put("ecirc","234");
    entityMap.put("euml","235");
    entityMap.put("igrave","236");
    entityMap.put("iacute","237");
    entityMap.put("icirc","238");
    entityMap.put("iuml","239");
    entityMap.put("eth","240");
    entityMap.put("ntilde","241");
    entityMap.put("ograve","242");
    entityMap.put("oacute","243");
    entityMap.put("ocirc","244");
    entityMap.put("otilde","245");
    entityMap.put("ouml","246");
    entityMap.put("divide","247");
    entityMap.put("oslash","248");
    entityMap.put("ugrave","249");
    entityMap.put("uacute","250");
    entityMap.put("ucirc","251");
    entityMap.put("uuml","252");
    entityMap.put("yacute","253");
    entityMap.put("thorn","254");
    entityMap.put("yuml","255");
    entityMap.put("OElig","338");
    entityMap.put("oelig","339");
    entityMap.put("Scaron","352");
    entityMap.put("scaron","353");
    entityMap.put("Yuml","376");
    entityMap.put("fnof","402");
    entityMap.put("circ","710");
    entityMap.put("tilde","732");
    entityMap.put("Alpha","913");
    entityMap.put("Beta","914");
    entityMap.put("Gamma","915");
    entityMap.put("Delta","916");
    entityMap.put("Epsilon","917");
    entityMap.put("Zeta","918");
    entityMap.put("Eta","919");
    entityMap.put("Theta","920");
    entityMap.put("Iota","921");
    entityMap.put("Kappa","922");
    entityMap.put("Lambda","923");
    entityMap.put("Mu","924");
    entityMap.put("Nu","925");
    entityMap.put("Xi","926");
    entityMap.put("Omicron","927");
    entityMap.put("Pi","928");
    entityMap.put("Rho","929");
    entityMap.put("Sigma","931");
    entityMap.put("Tau","932");
    entityMap.put("Upsilon","933");
    entityMap.put("Phi","934");
    entityMap.put("Chi","935");
    entityMap.put("Psi","936");
    entityMap.put("Omega","937");
    entityMap.put("alpha","945");
    entityMap.put("beta","946");
    entityMap.put("gamma","947");
    entityMap.put("delta","948");
    entityMap.put("epsilon","949");
    entityMap.put("zeta","950");
    entityMap.put("eta","951");
    entityMap.put("theta","952");
    entityMap.put("iota","953");
    entityMap.put("kappa","954");
    entityMap.put("lambda","955");
    entityMap.put("mu","956");
    entityMap.put("nu","957");
    entityMap.put("xi","958");
    entityMap.put("omicron","959");
    entityMap.put("pi","960");
    entityMap.put("rho","961");
    entityMap.put("sigmaf","962");
    entityMap.put("sigma","963");
    entityMap.put("tau","964");
    entityMap.put("upsilon","965");
    entityMap.put("phi","966");
    entityMap.put("chi","967");
    entityMap.put("psi","968");
    entityMap.put("omega","969");
    entityMap.put("thetasym","977");
    entityMap.put("upsih","978");
    entityMap.put("piv","982");
    entityMap.put("ensp","8194");
    entityMap.put("emsp","8195");
    entityMap.put("thinsp","8201");
    entityMap.put("zwnj","8204");
    entityMap.put("zwj","8205");
    entityMap.put("lrm","8206");
    entityMap.put("rlm","8207");
    entityMap.put("ndash","8211");
    entityMap.put("mdash","8212");
    entityMap.put("lsquo","8216");
    entityMap.put("rsquo","8217");
    entityMap.put("sbquo","8218");
    entityMap.put("ldquo","8220");
    entityMap.put("rdquo","8221");
    entityMap.put("bdquo","8222");
    entityMap.put("dagger","8224");
    entityMap.put("Dagger","8225");
    entityMap.put("bull","8226");
    entityMap.put("hellip","8230");
    entityMap.put("permil","8240");
    entityMap.put("prime","8242");
    entityMap.put("Prime","8243");
    entityMap.put("lsaquo","8249");
    entityMap.put("rsaquo","8250");
    entityMap.put("oline","8254");
    entityMap.put("frasl","8260");
    entityMap.put("euro","8364");
    entityMap.put("image","8465");
    entityMap.put("weierp","8472");
    entityMap.put("real","8476");
    entityMap.put("trade","8482");
    entityMap.put("alefsym","8501");
    entityMap.put("larr","8592");
    entityMap.put("uarr","8593");
    entityMap.put("rarr","8594");
    entityMap.put("darr","8595");
    entityMap.put("harr","8596");
    entityMap.put("crarr","8629");
    entityMap.put("lArr","8656");
    entityMap.put("uArr","8657");
    entityMap.put("rArr","8658");
    entityMap.put("dArr","8659");
    entityMap.put("hArr","8660");
    entityMap.put("forall","8704");
    entityMap.put("part","8706");
    entityMap.put("exist","8707");
    entityMap.put("empty","8709");
    entityMap.put("nabla","8711");
    entityMap.put("isin","8712");
    entityMap.put("notin","8713");
    entityMap.put("ni","8715");
    entityMap.put("prod","8719");
    entityMap.put("sum","8721");
    entityMap.put("minus","8722");
    entityMap.put("lowast","8727");
    entityMap.put("radic","8730");
    entityMap.put("prop","8733");
    entityMap.put("infin","8734");
    entityMap.put("ang","8736");
    entityMap.put("and","8743");
    entityMap.put("or","8744");
    entityMap.put("cap","8745");
    entityMap.put("cup","8746");
    entityMap.put("int","8747");
    entityMap.put("there4","8756");
    entityMap.put("sim","8764");
    entityMap.put("cong","8773");
    entityMap.put("asymp","8776");
    entityMap.put("ne","8800");
    entityMap.put("equiv","8801");
    entityMap.put("le","8804");
    entityMap.put("ge","8805");
    entityMap.put("sub","8834");
    entityMap.put("sup","8835");
    entityMap.put("nsub","8836");
    entityMap.put("sube","8838");
    entityMap.put("supe","8839");
    entityMap.put("oplus","8853");
    entityMap.put("otimes","8855");
    entityMap.put("perp","8869");
    entityMap.put("sdot","8901");
    entityMap.put("lceil","8968");
    entityMap.put("rceil","8969");
    entityMap.put("lfloor","8970");
    entityMap.put("rfloor","8971");
    entityMap.put("lang","9001");
    entityMap.put("rang","9002");
    entityMap.put("loz","9674");
    entityMap.put("spades","9824");
    entityMap.put("clubs","9827");
    entityMap.put("hearts","9829");
    entityMap.put("diams","9830");    
  }
  
  
  /**
   * @param p: pubgroup
   * @return true if either the pubgroup has a parent pubgroup or has no parent
   *         and also has no child pubgroups, and false otherwise.
   */
  public static boolean pubgroupHasParentOrIsChildless(Pubgroup p) {
    return (p.getParentPubgroup() == null && p.getChildren().isEmpty()) ||
        p.getParentPubgroup() != null;
  }
}

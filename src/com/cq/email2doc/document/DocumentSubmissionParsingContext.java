package com.cq.email2doc.document;

public class DocumentSubmissionParsingContext
{
  private boolean verbose;
  private boolean reply = false;
  private boolean forward = false;
  
  public boolean isVerbose()
  {
    return verbose;
  }
  
  public void setVerbose(
    boolean verbose)
  {
    this.verbose = verbose;
  }
  
  public boolean isReply()
  {
    return reply;
  }
  
  public void setReply(
    boolean reply)
  {
    this.reply = reply;
  }
  
  public boolean isForward()
  {
    return forward;
  }
  
  public void setForward(
    boolean forward)
  {
    this.forward = forward;
  }
  
  
  
}
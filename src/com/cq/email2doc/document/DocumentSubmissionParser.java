package com.cq.email2doc.document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.htmlparser.jericho.Attributes;
import net.htmlparser.jericho.CharacterReference;
import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.EndTag;
import net.htmlparser.jericho.EndTagType;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Segment;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.StartTagType;
import net.htmlparser.jericho.Tag;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cq.email2doc.Util;
import com.cq.email2doc.mail.EmailSubmission;
import com.cq.email2doc.mail.EmailSubmissionPart;
import com.cq.service.document.utils.ReporterManager;

/**
 * Parses an EmailSubmission into a DocumentSubmission, determining logical
 * parts such as hed, subhed, byline, grafs.
 * 
 */

public class DocumentSubmissionParser
{
  private static Log log = LogFactory.getLog(DocumentSubmissionParser.class);
  
  private static final Pattern fromMailtoEmailPattern = Pattern.compile("<(.+?)<mailto:(.+?)>>");
  private static final Pattern fromEmailPattern = Pattern.compile("<(.+?)>");

  private static final Pattern subjectForForwardPattern = Pattern.compile("(FW:|FWD:)", Pattern.CASE_INSENSITIVE);

  private ReporterManager reporterManager;
  
  /**
   * Parses an EmailSubmission into a DocumentSubmission, determining logical
   * parts such as hed, subhed, byline, grafs.
   */
  public DocumentSubmission parse(
    DocumentSubmissionParsingContext context,
    EmailSubmission submission )
  {
    DocumentSubmission doc = new DocumentSubmission();
    doc.setMailboxType( submission.getMailboxType() );
        
    parseSubjectIntoHed( context, trimOrNull( submission.getSubject() ), doc );
    
    doc.setFromEmailAddress( parseEmailAddress( trimOrNull( submission.getFrom() ) ) );
    
    String content = null;
    
    EmailSubmissionPart part = submission.findPart("text/html");
    if (part == null)
      part = submission.findPart("TEXT/HTML");    
    if (part == null)
      part = submission.findPart("text/plain");
    if (part == null)
      part = submission.findPart("TEXT/PLAIN");
    
    System.out.println("Starting with part content:\n"+part.getContent());

    log.info("part.getContentType() = "+part.getContentType());

    // If the email provides a HTML text variant, prefer that for now.    
    if ( part.getContentType().startsWith( "text/html" ) || part.getContentType().startsWith( "TEXT/HTML" ))
    {
      content = removeEndlineMarkers( part.getContent() );
      content = "<body>" + content + "</body>";
      content = parseHtmlBody( context, content ); 
    }
    else if ( part.getContentType() == null || part.getContentType().startsWith( "text/plain" ) || part.getContentType().startsWith( "TEXT/PLAIN") )
    {
      log.info("Calling stripNonAsciiContent for " + part.getContent());
      content = stripNonAsciiContent( part.getContent() );
    }
    
    content = removeForwardIndentationMarkers( content );

    content = removeCommonNoisePhrases( content );

    content = removeSignatures( context, content );
    
    List<String> nonBlankLines = new ArrayList<String>();
    
    for (String line: content.split("\n"))
    {
      // In parsing the body we treat each "line" as paragraph.
      // Blank lines are skipped.      
      line = trimOrNull( line );
      
      if (line != null)
        nonBlankLines.add( line );    
    }
    
    if (nonBlankLines.size() == 0 && doc.getHed() != null)
      nonBlankLines.add(doc.getHed());
    else if (nonBlankLines.size() > 0 && doc.getHed() == null)
    {
      String firstLine = nonBlankLines.get(0);
      
      if (firstLine.length() > 30)
        doc.setHed(nonBlankLines.get(0).substring(0,30));
      else
        doc.setHed(nonBlankLines.get(0));
    }
    
    if (doc.getHed() == null && nonBlankLines.size() == 0)
    {
      doc.addError( "Your email submission was empty. It was not processed.\n\nPlease submit an email with a headline and/or body text." );

      return doc;
    }
    
    if ( submississionRequiresSlug( submission )
         && nonBlankLines.size() > 0)
    {
      String firstLine = nonBlankLines.remove( 0 );
      String slug = Util.removeNonSlugCharacters( firstWordOfLine( firstLine ) );
      
      if ( Util.specified( slug ) )
        doc.setSlug( slug ); 
    }
    
    
    if ( submississionRequiresSlug( submission ) 
        && !Util.specified( doc.getSlug() ) )
    {
      doc.addError( "No slug specified.\n\nPlease submit an email with slug in the first line of the email body." );
    }

    if ( submississionRequiresSlug( submission ) 
        && nonBlankLines.size() == 0 )
    {
      doc.addError( "First line converted to slug, but rest of email was empty.\n\nPlease submit an email with slug in the first line of the email body." );
    }
    
    log.info("Before adding graf: "+ nonBlankLines);

    for (String line: nonBlankLines)
    {
      doc.addGraf( line );    
    }

    return doc;
  }

  /**
   * 
   * @param src
   * @return
   */
  private String removeEndlineMarkers( 
    String src )
  {
    StringBuilder buf = new StringBuilder();

    for (String line: src.split("\n"))
    {
      if (line.endsWith("="))
      {
        line = line.substring(0, line.length() - 1);
        buf.append( line );
      }
      else
      {
        buf.append( line ).append( " \n" );
      }
    }
      
    return buf.toString();
  }

  /**
   * 
   * @param src
   * @return
   */
  private String removeForwardIndentationMarkers( 
    String src )
  {
    StringBuilder buf = new StringBuilder();
    
    for (String line: src.split("\n"))
    {
      if (line.startsWith(">"))
      {
        int pos = 1;
        while (pos < line.length() && (line.charAt(pos) == '>' || line.charAt(pos)==' '))
          pos++;
        line = line.substring(pos);
      }
      buf.append( line ).append( "\n" );      
    }
      
    return buf.toString();
  }
  
  private String removeCommonNoisePhrases(
    String src)
  {
    StringBuilder buf = new StringBuilder();
    
    String[] thingsToLookFor = {
      "Begin forwarded message:", 
      "Download the official Twitter",
      "From:", "Subject:", "Date:", "To:", 
      "This e-mail may contain confidential material.",
      "------"
    };
    
    for (String line: src.split("\n"))
    {
      boolean skip = false;
      
      for (String thing: thingsToLookFor)
      {
        if (line.startsWith( thing ))
        {
          skip = true;
          break;
        }
      }
      
      if ( skip )
        continue;
      
      buf.append( line ).append( "\n" );      
    }
      
    return buf.toString();
  }
  
  
  /**
   * 
   * @param src
   * @return
   */
  private String removeSignatures(
    DocumentSubmissionParsingContext context,
    String src)
  {
   // if (context.isVerbose())
   //   System.out.println("removeSignatures: starting with content:\n"+src);
    
    StringBuilder buf = new StringBuilder();
            
    boolean inSignatureBlock = false;
    
    for (String line: src.split("\n"))
    {
      
      if (!inSignatureBlock)
      {
        for (String name: reporterManager.getReporterSignatures())
        {
          if (log.isDebugEnabled())
            log.debug("removeSignatures: checking line for signature: "+name);
          
          if (line.startsWith( name ))
          {          
            inSignatureBlock = true;
            break;
          }
        }
      }
      else 
      {
        // If we are in the signature block, then we must be ending
        // the signature block if there is a blank line.
        if (this.trimOrNull(line) == null)
          inSignatureBlock = false;
      }
      
      if ( inSignatureBlock )
        continue;
      
      buf.append( line ).append( "\n" );      
    }
      
    return buf.toString();
  }

  
  /**
   * 
   * @param src
   * @return
   */
  private String removePressReleaseIntro(
    String src)
  {
    // When found in a plaintext part, strip out "FOR IMMEDIATE RELEASE" and next lines
    // up to a blank line.
    //
    // Example:
    //FOR IMMEDIATE RELEASE
    //September 30, 2013      CONTACTS:    Mike Saccone (Udall) -- 202-224-4334
    //Adam Bozzi (Bennet) -- 202-228-5905
    
    String introStartText = "FOR IMMEDIATE RELEASE";
    
    StringBuilder buf = new StringBuilder();
    
            
    boolean inPressIntroBlock = false;
    
    for (String line: src.split("\n"))
    {
      
      if (!inPressIntroBlock)
      {
        if (line.startsWith( introStartText ))
        {          
          inPressIntroBlock = true;
          break;
        }
      }
      else 
      {
        // If we are in the block, then we must be ending
        // the block if there is a blank line.
        if (this.trimOrNull(line) == null)
          inPressIntroBlock = false;
      }
      
      if ( inPressIntroBlock )
        continue;
      
      buf.append( line ).append( "\n" );      
    }
      
    return buf.toString();
  }

  
  
  /** 
   * Parse the email subject into a headline, with special handling
   * to strip out reply headers (RE:) and forward headers (FW:)
   * <p>
   * If either a reply or a forward is detected then the context is
   * updated.
   * 
   * @param context
   * @param subject
   * @param doc
   */
  
  private void parseSubjectIntoHed(
    DocumentSubmissionParsingContext context,
    String subject,
    DocumentSubmission doc)
  {
    if ( subject == null )
      return;
    
    // Check for reply:  RE:
    
    if ( subject.matches( ".*RE:.*" ) )
    {
      subject = subject.replaceAll("(?i)RE:", "");
      context.setReply( true );      
    }
    

    // Check for forwards:   FW: or FWD:

    Matcher matcher = subjectForForwardPattern.matcher( subject );
    
    if ( matcher.find() )
    {
      context.setForward( true );      

      subject = subject.replaceAll("(?i)FW:", "");
      subject = subject.replaceAll("(?i)FWD:", "");
    }
    
    subject = trimOrNull( subject );
    
    doc.setHed( subject );
  }
  
  
  /**
   * 
   * @param content
   * @return
   */
  private String parseHtmlBody(
    DocumentSubmissionParsingContext context,
    String content)
  {
    Source source = new Source(content);
    StringBuilder sb = new StringBuilder();

    List<Element> startElements = source.getAllElements(HTMLElementName.BODY);

    if (startElements == null || startElements.size() ==0)
      startElements = source.getAllElements(HTMLElementName.DIV);
      
    if (startElements != null && startElements.size() > 0)
    {
      boolean lastNodeWasATextSegment = false;

      for (Element elem: startElements)
      {
        for (Iterator nodeIterator = elem.getNodeIterator(); nodeIterator.hasNext();)
        {
          Segment nodeSegment = (Segment) nodeIterator.next();
          if (nodeSegment instanceof Tag)
          {
            Tag tag = (Tag) nodeSegment;
            
            if (tag.getTagType().isServerTag())
              continue; // ignore server tags
            // Process the tag (just output it in this example):
    
            if ((tag.getName().equals(HTMLElementName.BR)
                 || tag.getName().equals(HTMLElementName.DIV)
                 || tag.getName().equals(HTMLElementName.H4)
                 || tag.getName().equals(HTMLElementName.P)
                 || tag.getName().equals(HTMLElementName.LI))
                && tag.getTagType() instanceof StartTagType)
            {
              //System.out.println("=== tag="+tag.getName()+", type="+tag.getTagType().getDescription()+", adding newline");
              sb.append("\n");
            }
            
            if (tag.getName().equals(HTMLElementName.A)
                && tag.getTagType() instanceof StartTagType)
            {
              StartTag startTag = (StartTag) tag;
              Attributes attributes = startTag.getAttributes();
              Map <String, String> attributesMap = new HashMap<String,String>();
              attributes.populateMap(attributesMap, true);
              String text = StartTag.generateHTML(startTag.getName(), attributesMap, false);
              sb.append(text);
            }

            if (tag.getName().equals(HTMLElementName.A)
                && tag.getTagType() instanceof EndTagType)
            {
              EndTag endTag = (EndTag) tag;
              String text = EndTag.generateHTML(endTag.getName());
              sb.append(text);
            }
            
            if (!tag.getName().equals(HTMLElementName.SPAN))
              lastNodeWasATextSegment = false;

          }
          else
          {
            // Segment is a text segment.
            // Process the text segment (just output its text in this example):
            //String text = CharacterReference.decodeCollapseWhiteSpace(nodeSegment);
            String text = CharacterReference.decode(nodeSegment);
            
            if (text != null)
              text = text.replace("\n", "");
            if (text != null)
              text = text.replace("\r", "");

            //if (context.isVerbose())
            //  System.out.println("parseHtmlBody: text segment=/"+text+"/");

            log.info("parseHtmlBody: text segment=/"+text+"/");
            
            //if (context.isVerbose())
            //  System.out.println("parseHtmlBody: text2 segment=/"+text2+"/");

            if (!lastNodeWasATextSegment)
            {
              if (sb.length() > 0 && sb.charAt(sb.length()-1) != '\n')
                sb.append(" ");
            }
            sb.append(text);
            
            lastNodeWasATextSegment = true;
          }
        }
      }
    }

    log.info("sb.toString() = " + sb.toString());

    // Collapse multiple spaces or tabs into a single space and return the result.
    return sb.toString().replaceAll("[ \t]", " ");
  }  
  
  
  /**
   * 
   * @param content
   * @return
   */
  private String stripReporterSignature(
    String content)
  {
    Pattern pattern = Pattern.compile("(^-{3}$)", Pattern.DOTALL
        | Pattern.MULTILINE);
    Matcher matcher = pattern.matcher(content);
    String newContent = content;
    int start = 0;

    if (matcher.find())
    {
      start = matcher.start();
      newContent = content.substring(0, start);

    }

    return newContent;
  }


  private String stripNonAsciiContent(
    String content)
  {
    content = content.replaceAll("\\R\\R", "--br--");
    content = content.replaceAll("\\R", " ");
    content = content.replace("--br--", "\n\n");
    return content.replaceAll("[^\\x00-\\x7f]", "");
  }
  
  /**
   * Parse out the email address from the "from" header.
   * 
   * @param from
   * @return
   */
  private String parseEmailAddress(
		String from)
  {
    if ( from == null )
      return null;
    
    // Some possibilities:
    //   tom thumb <tomthumb@gamil.com>
    //   tomthumb@gmail.com
    
    // There is even this variant with <mailto:EMAIL> inside of the email address weird.
    //   Niels Lesniewski <NielsLesniewski@cqrollcall.com<mailto:NielsLesniewski@cqrollcall.com>>
    
    // If we match the NAME<EMAILADDRESS> pattern, then 
    // extract the email address between the angled brackets,
    // otherwise return the entire from header.
    
    Matcher matcher1 = fromMailtoEmailPattern.matcher( from );
    
    if (matcher1.find())
      return matcher1.group(1);
    
	  Matcher matcher2 = fromEmailPattern.matcher( from );
	  
	  if (matcher2.find())
  	  return matcher2.group(1);
	  else
	    return from.trim();
  }
  
  
  /**
   * Return the string trimmed of whitespace, and return null if the string
   * ends up empty.
   * 
   * @param src
   * @return
   */
  private String trimOrNull( 
    String src)
  {
    if (src != null)
    {
      src = src.trim();
      
      if (src.length() == 0)
        src = null;
    }
    
    return src;
  }
  
  private String firstWordOfLine(String text)
  {
    if (text == null)
      return null;
    
    return text.split(" ")[0];
  }


  public ReporterManager getReporterManager()
  {
    return reporterManager;
  }

  public void setReporterManager(
    ReporterManager reporterManager)
  {
    this.reporterManager = reporterManager;
  }
  
  private boolean submississionRequiresSlug(
    EmailSubmission submission)
  {
    if ( submission.getMailboxType() != null 
        && ( submission.getMailboxType().equalsIgnoreCase("news") 
            || submission.getMailboxType().equalsIgnoreCase("healthbeat")
            ||submission.getMailboxType().equalsIgnoreCase("committeenews") ) )
    {
      return true;  
    }
    
    return false;
  }
  
}

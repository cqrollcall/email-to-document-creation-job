package com.cq.email2doc.document;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.text.translate.CharSequenceTranslator;
import org.apache.commons.lang3.text.translate.NumericEntityEscaper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.cq.Util;
import com.cq.email2doc.CongressInfoProvider;
import com.cq.email2doc.reporter.ReporterFinder;
import com.cq.service.document.utils.DocumentTextManager;
import com.cq.service.udr.UdrBillService;
import com.cq.service.udr.UdrDocumentService;
import com.cq.service.udr.UdrService;
import com.cq.udr.Bill;
import com.cq.udr.BillBriefTitle;
import com.cq.udr.Congress;
import com.cq.udr.CqLiveDestination;
import com.cq.udr.DateAccuracy;
import com.cq.udr.Document;
import com.cq.udr.DocumentByline;
import com.cq.udr.DocumentDestination;
import com.cq.udr.DocumentHistory;
import com.cq.udr.DocumentProductCycle;
import com.cq.udr.DocumentProductLink;
import com.cq.udr.DocumentSection;
import com.cq.udr.DocumentSource;
import com.cq.udr.DocumentStatus;
import com.cq.udr.DocumentText;
import com.cq.udr.DocumentVersion;
import com.cq.udr.Product;
import com.cq.udr.ProductCycle;
import com.cq.udr.PublicLaw;
import com.cq.udr.PublicLawShortTitle;
import com.cq.udr.Reporter;
import com.cq.udr.WorkflowType;


public class DocumentLoaderImpl
  implements DocumentLoader
{
  private static Log log = LogFactory.getLog(DocumentLoaderImpl.class);
  
  @Resource
  private HibernateTemplate udrHibernateTemplate;
    
  @Resource
  private UdrService udrService;
  
  @Resource
  private UdrDocumentService udrDocumentService;
  
  @Resource
  private ReporterFinder reporterFinder;
  
  @Resource
  private TransactionTemplate udrTransactionTemplate;
  
  @Resource
  private DocumentTextManager documentTextManager;
  
  @Resource
  private UdrBillService udrBillService;
  
  @Resource
  private CongressInfoProvider congressInfoProvider;
    
  
  private static CharSequenceTranslator xmlEscaper
    = StringEscapeUtils.ESCAPE_XML.with( NumericEntityEscaper.between(0x7f, Integer.MAX_VALUE) );

  SimpleDateFormat mmmdotddyyyy = new SimpleDateFormat("MMM. dd, yyyy");
  SimpleDateFormat mmmddyyyy = new SimpleDateFormat("MMM dd, yyyy");
  
  /**
   * 
   */
  @Override
  public Document insertDocument(
    final DocumentSubmission docSubmission,
    final String principalUserName )
  {
    return 
    udrHibernateTemplate.execute( new HibernateCallback<Document>()
    {
      @Override
      public Document doInHibernate( Session session ) throws HibernateException, SQLException
      {
        Document doc = udrTransactionTemplate.execute( new TransactionCallback<Document>() 
        {

          @Override
          public Document doInTransaction( TransactionStatus txStatus )
          {
            Date now = new Date();
            
            DocumentSource source = calculateDocumentSource( docSubmission );
            DocumentSection section = calculateDocumentSection( docSubmission );
      
            Document doc = new Document();
            doc.setCreateDate( now );
            doc.setCreateBy( principalUserName );
            doc.setUpdateDate( now );
            doc.setUpdateBy( principalUserName );
            doc.setUpdateCounter(0L);
            doc.setCongress( new Long( congressInfoProvider.getCurrentCongress() ));
            doc.setDocumentStatus( DocumentStatus.find( DocumentStatus.Enum.NEW ) );
            doc.setWorkflowType( WorkflowType.find( WorkflowType.Enum.EDIT ) );
            doc.setSource( source );
            doc.setSection( section );
            doc.setHeadline( docSubmission.getHed() );
            doc.setPubDateAccuracy( udrDocumentService.getDateAccuracyType( DateAccuracy.DATE_ACCURACY_DAY ) );
            doc.setWordCount( Long.valueOf( docSubmission.getWordCount() ) );
            
            if ( Util.specified( docSubmission.getSlug() ) )
              doc.setSourceFile( docSubmission.getSlug() );
            
            log.info("Before Transform : " + docSubmission.getSectionList());

            String text = transformToNewsStoryXml( docSubmission );
            
            log.info("After Transform : " + text);
            
            String abstractText = null;
            try
            {
              abstractText = documentTextManager.getDocumentExcerpt( text, 300 ).trim();
              doc.setAbstractText( abstractText );
            }
            catch(Exception e)
            {
              log.warn(e);
            }
            
            DocumentText docText = new DocumentText();
            docText.setCreateDate( now );
            docText.setCreateBy( principalUserName );
            docText.setUpdateDate( now );
            docText.setUpdateBy( principalUserName );
            docText.setUpdateCounter(0L);
            doc.setDocumentText( docText );
            docText.setText( text );
            
            DocumentProductLink link = new DocumentProductLink();
            
            if ( source.getId() == DocumentSource.CQHEALTH)
            {
              link.setProduct( udrService.getProduct( Product.HEALTHBEAT ) );
            }
            else
            {
              link.setProduct( udrService.getProduct( Product.CQCOM ) );
            }
            link.setCreateBy( principalUserName );
            link.setCreateDate( now );
            link.setUpdateCounter( 0L );
            doc.addDocumentProduct( link );

            // CQ Live stories don't get a byline.
            if ( source.getId() != DocumentSource.CQLIVE)
            {
              ProductCycle btProductCycle = udrService.findProductCycleForDate(4l, now);
              if (btProductCycle != null)
              {
                DocumentProductCycle dpc = new DocumentProductCycle();
                dpc.setCreateBy( principalUserName );
                dpc.setCreateDate( now );
                dpc.setUpdateCounter( 0L );
            
                dpc.setStartDate( now );
                dpc.setDocument(doc);
                dpc.setProductCycle(btProductCycle);
                doc.getDocumentProductCycles().add(dpc);
              }
              
              Reporter reporter = reporterFinder.findReporterByAlias( docSubmission.getFromEmailAddress() );
  
              if (reporter != null)
              {
                DocumentByline byline = new DocumentByline();
                byline.setCreateBy( principalUserName );
                byline.setCreateDate( now );
                byline.setUpdateCounter( 0L );
                byline.setSortSeq(1L);
            
                byline.setDocument(doc);
                doc.getDocumentBylines().add(byline);
                
                byline.setReporter(reporter);
                byline.setReporterName( reporterFinder.convertToBylineString(reporter) );
              }  
              else
              {
                log.warn("Could not guess reporter to assign initial byline for email address: "+docSubmission.getFromEmailAddress());
              }
            }
         
            
            /* Set a default DocumentDestination link for the document if it requires it */
            /* Set a default DocumentDestination link for the document if it requires it */
            if ( com.cq.email2doc.Util.sourceToDestinationNameMap.containsKey( source.getId() ) )
            {
              DocumentDestination destLink = new DocumentDestination();
              destLink.setDestination( CqLiveDestination.findByName( com.cq.email2doc.Util.sourceToDestinationNameMap.get( source.getId()) ) );    
              destLink.setCreateBy( principalUserName );
              destLink.setCreateDate( now );
              destLink.setUpdateCounter( 0L );
              doc.addDocumentDestination( destLink );      
            }
            
            DocumentHistory entry = new DocumentHistory();
            entry.setCreateDate( now );
            entry.setCreateBy( principalUserName );
            entry.setUpdateDate( now );
            entry.setUpdateBy( principalUserName );
            entry.setUpdateCounter(0L);
            doc.addDocumentHistory( entry );
            entry.setVersion( 1L );
            entry.setWorkflowType( doc.getWorkflowType() );
            entry.setDocumentStatus( doc.getDocumentStatus() );  
            
            DocumentVersion dv = new DocumentVersion();
            dv.setCreateBy( doc.getCreateBy() );
            dv.setCreateDate( now );
            dv.setDocument( doc );
            dv.setVersion( 0 );  // how should we maintain this?
            dv.setNote( null );
            dv.setWorkflowType( doc.getWorkflowType() );
            dv.setDocumentStatus( doc.getDocumentStatus() );
            dv.setContentType( "text" );  // placeholder.
            dv.setContent( doc.getDocumentText().getText() );            
            doc.addDocumentVersion( dv );
                        
            udrHibernateTemplate.saveOrUpdate( doc );
            
            return doc;
          }
        });
        
        return doc;
      }
    });
  }
  
  private DocumentSource calculateDocumentSource(DocumentSubmission submission)
  {
    if (submission.getMailboxType() != null && submission.getMailboxType().equalsIgnoreCase("news"))
      return (DocumentSource)udrService.loadEntity( DocumentSource.class, DocumentSource.CQNEWS );
    else if (submission.getMailboxType() != null && submission.getMailboxType().equalsIgnoreCase("healthbeat"))
      return (DocumentSource)udrService.loadEntity( DocumentSource.class, DocumentSource.CQHEALTH );
    else if (submission.getMailboxType() != null && submission.getMailboxType().equalsIgnoreCase("committeenews"))
      return (DocumentSource)udrService.loadEntity( DocumentSource.class, DocumentSource.COMMITTEENEWS );
    else
      return (DocumentSource)udrService.loadEntity( DocumentSource.class, DocumentSource.CQLIVE );
  }

  private DocumentSection calculateDocumentSection(DocumentSubmission submission)
  {
    if (submission.getMailboxType() != null && submission.getMailboxType().equalsIgnoreCase("news"))
      return (DocumentSection)udrService.loadEntity( DocumentSection.class, DocumentSection.NEWS );
    else if (submission.getMailboxType() != null && submission.getMailboxType().equalsIgnoreCase("healthbeat"))
      return (DocumentSection)udrService.loadEntity( DocumentSection.class, DocumentSection.ARTICLES );
    else if (submission.getMailboxType() != null && submission.getMailboxType().equalsIgnoreCase("committeenews"))
      return (DocumentSection)udrService.loadEntity( DocumentSection.class, DocumentSection.NEWS );
    else
      return (DocumentSection)udrService.loadEntity( DocumentSection.class, DocumentSection.NA );
  }
  
  private String transformToNewsStoryXml(
    DocumentSubmission documentSubmission)
  {
    StringBuilder sb = new StringBuilder();
    /*
       <news-story>
         <front>
            <kikker>ENVIRONMENT</kikker>
            <news-title>State Department Finds Pipeline Would Have Limited Impact </news-title>
         </front>
         <body>
            <section>
     */
    

    sb.append("<news-story>\n");

    sb.append("<front>\n");
    sb.append("</front>\n");
    
    sb.append("<body>\n");
    
    if ( documentSubmission.getSectionList() != null)
    {
      for (Section section: documentSubmission.getSectionList())
      {
        sb.append("<section>\n");
        for (String graf: section.getGrafs())
        {
          sb.append("<p>");
          
          sb.append( xmlEscaper.translate( graf ) );
          
          sb.append("</p>");
        }
        sb.append("</section>\n");
      }
    }
    
    sb.append("</body>\n");
    sb.append("</news-story>\n");
    
    return sb.toString() ;
    
  }
  
 
  @Override
  @Transactional
  public void insertPLRecord(List<PublicLawData> plList)
  {
    
    Set<String> processedBillNumbers = new HashSet();
    
    for (PublicLawData pld: plList)
    {
      // **** ALF-5605 Remove duplicate PLs being inserted 
      if ( processedBillNumbers.contains(pld.getBillNumber() ) )
        continue;
      processedBillNumbers.add( pld.getBillNumber());

      log.info("+++++ Inserting Public Law for : "+pld.getBillNumber());
      
      Date enactedDate = new Date();
      SimpleDateFormat  yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
      try {
        enactedDate = yyyyMMdd.parse(pld.lawDate);
      }
      catch (Exception e)
      {
        enactedDate =new Date(); // Fall back
      }
      int sessionNumber = getSessionNumber(  enactedDate);
      PublicLaw plaw = new PublicLaw();
      // cleanup bill number
      String billnum = pld.getBillNumber().toLowerCase().trim() ;
      billnum = billnum.replace(".", "");
      billnum = billnum.replace(" ", "");
      
      String lType ="An Act";
      if (billnum !=null && ( billnum.startsWith("hjres") || billnum.startsWith("sjres") ) )
        lType="Joint Resolution";
      plaw.setBillNumber( billnum );
      plaw.setCongressNumber( Long.parseLong(pld.getCongress() ) );
      plaw.setLawNumber( pld.getLawNumber());
      plaw.setOfficialTitle( pld.getTitle().trim());
      plaw.setPageRange( pld.getPageRange().trim());
      plaw.setStatPage( pld.getStatPage().trim());
      plaw.setLegisType(lType); // Fix me ????
      plaw.setSessionNumber( new Long(sessionNumber ) ); 
      plaw.setLawDate(enactedDate ); 
      
      // Short titles
      List<PublicLawShortTitle> lawShortTitles = new ArrayList();
      Bill  bill = null;
      try {
       bill = udrBillService.getBillByCongressAndBillNumber(Long.parseLong(pld.getCongress() ), billnum);
      }
      catch (Exception e)
      {
        log.error("Could not find bill number "+billnum);
        bill = null;
      }
      
      if (bill !=null)
      {
        try
        {
          Set<BillBriefTitle> stitles = bill.getBriefTitles();
          for (BillBriefTitle bbt: stitles)
          {
            String btitle = bbt.getBriefTitle();
            if (! btitle.equalsIgnoreCase(plaw.getOfficialTitle())) {
              PublicLawShortTitle pls = new PublicLawShortTitle();
              pls.setShortTitle( btitle);
              pls.setLevelType(lType.equalsIgnoreCase("an act")?"act":"resolution");
              pls.setStatVol(pld.getVolume());
              pls.setPublicLaw(plaw);
              
              lawShortTitles.add( pls );
            }
          }
        }
        catch (Exception e)
        {
          log.error("Error in getting short titles for Law :"+pld.getLawNumber(),e);
        }
        if (! lawShortTitles.isEmpty())
          plaw.setShortTitles( lawShortTitles);
      }
      udrHibernateTemplate.saveOrUpdate( plaw );  
      udrHibernateTemplate.flush();
      log.info("+++++ Inserted Public Law for : "+pld.getBillNumber());
    }
    
  }

  private int getSessionNumber(
    Date enactedDate)
  {
    int result = 1; 
    try
    {// Assume session 1
    List<Congress> congresses = congressInfoProvider.getCongressForDate( enactedDate );
    if (congresses !=null && !congresses.isEmpty())
    {
      Congress cong = congresses.get( 0 );
      Set<com.cq.udr.Session> sessions = cong.getSessions();
      for (com.cq.udr.Session s : sessions)
      {
        if (enactedDate.after(s.getStartDate()) && enactedDate.before( s.getEndDate() ) )
        {
          result = s.getSessionNumber();
        }
      }
      
    }
    }
    catch( Exception e)
    {
      log.error("Could not get congress session ",e);
    }
    return result;
  }

       
  
}

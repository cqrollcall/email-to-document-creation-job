package com.cq.email2doc.document;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a document submission parsed from an email or other messaging system,
 * which is an intermediate step before inserting into the UDR.
 * <p>
 * The data has been broken down from the freeform text of an email into logical
 * parts: headline, section, grafs, byline.  The DocumentSubmission will
 * eventually be placed into the UDR DOCUMENT table (and DOCUMENT_* friends), but it 
 * does not yet have all of the rigorous UDR relations such as source, section, 
 * subject codes, etc.
 * 
 */

public class DocumentSubmission
{
  private String mailboxType;
  private String hed;
  private List<Section> sectionList;
  private Section currentSection = null;
  private String fromEmailAddress;
  private String slug;
  private List<String> errors = new ArrayList<String>();

  
  public String getMailboxType()
  {
    return mailboxType;
  }
  public void setMailboxType(
    String mailboxType)
  {
    this.mailboxType = mailboxType;
  }
  
  public String getHed()
  {
    return hed;
  }
  
  public void setHed(
    String hed)
  {
    this.hed = hed;
  }
  
  public int getWordCount()
  {
    int wordCount = 0;
      
    if ( getSectionList() != null )
    {
      for ( Section section: getSectionList() )
         wordCount += section.getWordCount();
    } 
    return wordCount;
  }

  public List<Section> getSectionList()
  {
    return sectionList;
  }
  
  public void setSectionList(
    List<Section> sectionList)
  {
    this.sectionList = sectionList;
  } 

  public void addGraf( 
    String graf )
  {
    if (currentSection == null)
    {
      currentSection = new Section();
      if (sectionList == null)
        sectionList = new ArrayList<Section>();
      sectionList.add( currentSection );
    }
    
    currentSection.addGraf( graf );
  }

  public String getFromEmailAddress()
  {
	  return fromEmailAddress;
  }
  
  public void setFromEmailAddress( 
		  String from)
  {
	  this.fromEmailAddress = from;
  }
  
  public String getSlug()
  {
    return slug;
  }
  
  public void setSlug(
    String slug)
  {
    this.slug = slug;
  }
  
  public void addError(String msg)
  {
    this.errors.add( msg );
  }
  
  public List<String> getErrors()
  {
    return errors;
  }
  
  public boolean hasErrors()
  {
    return errors.size() > 0;
  }
  
}

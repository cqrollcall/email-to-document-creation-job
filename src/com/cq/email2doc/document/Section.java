package com.cq.email2doc.document;

import java.util.ArrayList;
import java.util.List;

import com.cq.service.document.utils.WordCounter;

/**
 * Represents a section in a news document, basically a subhed (section title) 
 * and a list of grafs (paragrafs).
 */
public class Section
{
  private String subhed;
  private List<String> grafs = new ArrayList<String>();
  
  
  
  public String getSubhed()
  {
    return subhed;
  }
  
  public void setSubhed(
    String subhed)
  {
    this.subhed = subhed;
  }
  
  public List<String> getGrafs()
  {
    return grafs;
  }
  
  public void setGrafs(
    List<String> grafs)
  {
    if (grafs == null)
      this.grafs = new ArrayList<String>();
    else
      this.grafs = grafs;
  }
  
  public void addGraf(
    String graf )
  {
    if (grafs == null)
      grafs = new ArrayList<String>();
    
    grafs.add( graf );
  }

  public int getWordCount()
  {
    WordCounter counter = new WordCounter();
    
    int wordCount = counter.countWordsFromPlain( getSubhed() );
    
    for ( String graf: getGrafs() )
      wordCount += counter.countWordsFromPlain(graf);
    
    return wordCount;
  }

@Override
public String toString() {
	return "Section [subhed=" + subhed + ", grafs=" + grafs + "]";
}

}

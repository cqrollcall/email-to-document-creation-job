package com.cq.email2doc.document;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.cq.email2doc.mail.EmailSubmission;
import com.cq.email2doc.mail.SubmissionFetcher;
import com.cq.email2doc.slack.SlackNotifier;
import com.cq.email2doc.workflow.WorkflowNotificationParser;
import com.cq.udr.Document;

public class DocumentSubmissionJob
{
  private static Log log = LogFactory.getLog(DocumentSubmissionJob.class);
  
  @Resource
  private SlackNotifier slackNotifier;  
  
  private List<SubmissionFetcher> submissionFetchers;
  private DocumentSubmissionParser parser;
  private WorkflowNotificationParser workflowNotificationParser;
  private DocumentLoader loader;
  
  @Resource
  private JavaMailSender mailSender;
  
  SimpleDateFormat mmmdotddyyyy = new SimpleDateFormat("MMM. dd, yyyy");
  SimpleDateFormat mmmddyyyy = new SimpleDateFormat("MMM dd, yyyy");
  

  /**
   * 
   */
  public void checkForSubmissionsAndProcess()
  {
    List<EmailSubmission> submissions = new ArrayList<EmailSubmission>(); 
    
    for ( SubmissionFetcher fetcher: getSubmissionFetchers() )
    {
      List<EmailSubmission> submissionsForThisMailbox = fetcher.fetchSubmissions();
      
      if ( submissionsForThisMailbox != null && submissionsForThisMailbox.size() > 0 )
        submissions.addAll( submissionsForThisMailbox );
    }
    
    if ( submissions == null || submissions.size() == 0)
      return;
    
    log.info("after calling fetchSubmission, found "+submissions.size() +" emails");
    
    for (EmailSubmission submission: submissions)
    {
      log.info("***** Mail box name "+submission.getMailboxName()+", form "+submission.getFrom());
      // If the submission looks like a WordPress workflow notification, try that first.
      
      if ( ( submission.getFrom() !=null &&  submission.getFrom().toLowerCase().contains("wordpress") )
           || (submission.getSubject() !=null && submission.getSubject().toLowerCase().contains("wordpresstest") )  ) // so we can test just by sending in a fake in email
      {
        // do nothing
      }
      // Public Law ALF-3413 and ALF-3412 and ALF_3703
      else if ( (submission.getFrom() !=null && submission.getFrom().toLowerCase().contains("pens@gpo.gov") ) || 
          (submission.getSubject() !=null && submission.getSubject().toLowerCase().contains("public law") ) )
      {
        DocumentSubmissionParsingContext context = new DocumentSubmissionParsingContext();
        
        DocumentSubmission documentSubmission = parser.parse( context, submission );
        
        if ( documentSubmission.hasErrors() )
        {
          reportBadSubmissionToSender( submission, documentSubmission );
        }
        else
        {
          insertPublicLawRecord( documentSubmission );
        }
      }
      else if (! (submission.getFrom() !=null && submission.getFrom().toLowerCase().contains("google.com") ) ) // to avoid loading security alert emails from google
      {
        DocumentSubmissionParsingContext context = new DocumentSubmissionParsingContext();
        
        DocumentSubmission documentSubmission = parser.parse( context, submission );
        
        if ( documentSubmission.hasErrors() )
        {
          reportBadSubmissionToSender( submission, documentSubmission );
        }
        else
        {
          insertDocument( documentSubmission );
        }
      }
    }
  }
  
   private void insertPublicLawRecord(
    DocumentSubmission documentSubmission)
  {
    String head = documentSubmission.getHed();
    String congressAndLawNumbers = null;

  
    /*
     * S. 1436 / Public Law 116-21  
      To make technical corrections to the computation of average pay under Public Law 110-279.  
      (June 12, 2019; 133 Stat. 903; 2 pages)  
      The Pens email has the format 
      1. Bill number and law number
      2. Title
      3. Law date and page info
     */
    // These are the flags to collect the data
    boolean flagForTitle = false;    //
    boolean flagForStatPage = false;
    boolean flagForBill = true;
    
    List<PublicLawData> pensData = new ArrayList();
    String cong =""; ;
    if (head !=null)
    {
      if (head.toLowerCase().contains("public law") )
      {
        String headLower = head.toLowerCase();
        int i = headLower.indexOf("public law");
        int l = headLower.length();
        int sl = "public law".length();
        
        String temp = headLower.substring(i+sl+1);
        if (temp !=null)
        {
          congressAndLawNumbers = temp.trim();
        }
        if (congressAndLawNumbers !=null )
          {
            String lawData[] = congressAndLawNumbers.split("and");
            for (int index = 0; index < lawData.length; index++)
            {
              String[] values = congressAndLawNumbers.split("-");
              cong = values[0];
            }
          }
      }
    }
 // Initialize the data
    PublicLawData pData = null;
    
    List<Section> secList = documentSubmission.getSectionList();
    if (secList !=null && !secList.isEmpty())
    {
      for (Section sec : secList)
      {
        List<String> lines = sec.getGrafs();
        String title ="";
        for (String line : lines)
        {
          
          if (line !=null && line.length() > 0)
          {
            if (flagForBill)
              pData = new PublicLawData();
            if (line.toUpperCase().contains("PENS SUBSCRIPTION FAQS")) // All Law are processed
              break;
            if (line.toLowerCase().contains("public law") && flagForBill) 
            {
              extractBillAndLaw(pData,line);
              pData.setCongress( cong);
              flagForTitle = true;  // Ready for title
              flagForBill = false ; // Bill and law data collected 
            }
            else if (flagForTitle)
            {
              if (line !=null && line.length() > 0)
                {
                  String trimline = line.trim();
                  if ( !(trimline.startsWith("(") && trimline.endsWith(")") ) ) // title could span more than one line
                  {
                    if (title.length() > 0)
                      title+=" ";
                    title += trimline;
                  }
                  else
                  {
                     pData.setTitle( title );
                     title=""; // initializing the titl
                     extractDateAndPages(pData,line);
                     flagForBill = true; // Ready for next Law (if we have multiple laws in one email submission
                     flagForStatPage = false; // Page info done
                     flagForTitle = false;
                     pensData.add( pData);
                  }
                }
              }
        /*    else if (flagForStatPage )
            {
              extractDateAndPages(pData,line);
              flagForBill = true; // Ready for next Law (if we have multiple laws in one email submission
              flagForStatPage = false; // Page info done
              pensData.add( pData);
            } */
          }
        }
        
      }
      
    }
    
    for (PublicLawData p : pensData)
    {
      log.info("+++++ Public Law to be inserted: "+p.getBillNumber()+","+p.getLawDate()+","+p.getCongress()+","+p.getLawNumber()+","+p.getPageRange()+","+p.getStatPage());
    }
    loader.insertPLRecord(pensData );
    
  }


  private void extractDateAndPages(
  PublicLawData pData,
  String line)
{

    line = line.replace("(", "");
    line = line.replace(")", "");
    String[] parts = line.split(";");
    if (parts !=null && parts.length > 0)
    {
      String dateText = parts[0];
      String sPage = parts[1];
      String pr = parts[2].trim();
      String pageRange = parts[1];// fall back value
      // June 12, 2019; 133 Stat. 903; 2 pages)
      pData.setStatPage( (parts[1]));
      // get Page range
      if (pr !=null && pr.length() > 0)
      {
        String pnums = pr.substring(0,pr.indexOf(" "));
        // (Dec. 27, 2020; 134 Stat. 1182; 2,124 pages) 2,124 fails - so replace ,
        String cleanedpages = pnums.trim().replace(",", "");
        int pnumint = Integer.parseInt(cleanedpages);
        int po = sPage.indexOf("Stat.");
        String volume = sPage.substring(0,sPage.indexOf(" ")).trim();
        pData.setVolume(volume);
        sPage = sPage.substring( po + "Stat.".length() );
        int startPage = Integer.parseInt( sPage.trim());
        int endPage = startPage + pnumint -1 ;
        if (startPage == endPage)
          pageRange = ""+startPage;
        else
          pageRange = startPage +"-"+endPage ;
      }
      pData.setPageRange( pageRange);
      Date date1 = new Date();
      
      try {
        date1 = mmmdotddyyyy.parse(dateText);
      }
      catch (Exception e1)
      {
        try {
          date1 = mmmddyyyy.parse(dateText);
        }
        catch (Exception e2)
        {
          log.error("could not parse law date ",e2);
        }
      }
      SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
      pData.setLawDate(df.format(date1));
    }
  }


  private void extractBillAndLaw(
  PublicLawData pData,
  String line)
  {
  /// *** H.R. 1222 / Public Law 116-17

    String[] billlaw = line.split("/");
    if (billlaw !=null && billlaw.length > 0 )
    {
   // cleanup bill number
      String billnum = billlaw[0].toLowerCase().trim() ;
      billnum = billnum.replace(".", "");
      billnum = billnum.replace(" ", "");
      String lNum = billlaw[1].substring( billlaw[1].indexOf("-")+1);
      pData.setBillNumber( billnum);
      pData.setLawNumber( lNum );
    }
    
  }


  /**
   * 
   * @param documentSubmission
   */
  private void insertDocument( 
    DocumentSubmission documentSubmission )
  {
    try
    {
      Document doc = loader.insertDocument( documentSubmission, "system" );
      
      // Notify any other component in the application that we 
      // have accepted a document submission.
      
      slackNotifier.notify(doc, documentSubmission);
      
    }
    catch (Throwable t)
    {
      log.warn("unable to load document", t);
    }
    
  }
  

  
  
  
  private void reportBadSubmissionToSender(
    EmailSubmission submission,
    DocumentSubmission docSubmission )
  {
    if ( mailSender == null )
    {
      log.warn("emailSender is null, unable to report bad email submission to sender");
      return;
    }

    if ( submission.getFrom() == null )
    {
      log.warn("email submission FROM value is null, unable to report bad email submission to sender");
      return;
    }

    try
    {
      String subject = "WARNING: Email sent to "+submission.getMailboxName()+" not processed";
      
      StringBuilder body = new StringBuilder();
      for (String msg: docSubmission.getErrors())
      {
        if (body.length()> 0)
          body.append("\n");
        body.append(msg);
      }      
      
      SimpleMailMessage email = new SimpleMailMessage();
      email.setText( body.toString() );
      email.setReplyTo( "do-not-reply@cq.com" );
      email.setFrom( "do-not-reply@cq.com" );
      email.setTo( submission.getFrom().split(",") );
      email.setSubject( subject );
       
      mailSender.send(email);
      
      log.info("reported bad email submission vial email to "+submission.getFrom());
    }
    catch (Throwable t)
    {
      log.warn("unable to report bad email submission to sender", t);
    }
    
  }
  
  

  public DocumentSubmissionParser getDocumentSubmissionParser()
  {
    return parser;
  }

  public void setDocumentSubmissionParser(
    DocumentSubmissionParser parser)
  {
    this.parser = parser;
  }
  
  public WorkflowNotificationParser getWorkflowNotificationParser()
  {
    return workflowNotificationParser;
  }

  public void setWorkflowNotificationParser(
    WorkflowNotificationParser workflowNotificationParser)
  {
    this.workflowNotificationParser = workflowNotificationParser;
  }

  public DocumentLoader getDocumentLoader()
  {
    return loader;
  }

  public void setDocumentLoader(
    DocumentLoader loader)
  {
    this.loader = loader;
  }

  public List<SubmissionFetcher> getSubmissionFetchers()
  {
    return submissionFetchers;
  }

  public void setSubmissionFetchers(
    List<SubmissionFetcher> submissionFetchers)
  {
    this.submissionFetchers = submissionFetchers;
  }
  
  
}

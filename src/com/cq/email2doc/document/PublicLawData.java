package com.cq.email2doc.document;

public class PublicLawData 
{
  public String congress;
  public String billNumber;
  public String lawNumber;
  public String title;
  public String pageRange;
  public String statPage;
  public String lawDate;
  public String volume;
  
  public PublicLawData()
  {
    
  }

  public String getCongress()
  {
    return congress;
  }

  public void setCongress(
    String congress)
  {
    this.congress = congress;
  }

  public String getBillNumber()
  {
    return billNumber;
  }

  public void setBillNumber(
    String billNumber)
  {
    this.billNumber = billNumber;
  }

  public String getLawNumber()
  {
    return lawNumber;
  }

  public void setLawNumber(
    String lawNumber)
  {
    this.lawNumber = lawNumber;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(
    String title)
  {
    this.title = title;
  }

  public String getPageRange()
  {
    return pageRange;
  }

  public void setPageRange(
    String pageRange)
  {
    this.pageRange = pageRange;
  }

  public String getStatPage()
  {
    return statPage;
  }

  public void setStatPage(
    String statPage)
  {
    this.statPage = statPage;
  }

  public String getLawDate()
  {
    return lawDate;
  }

  public void setLawDate(
    String lawDate)
  {
    this.lawDate = lawDate;
  }

  public String getVolume()
  {
    return volume;
  }

  public void setVolume(
    String volume)
  {
    this.volume = volume;
  }
  
  

}

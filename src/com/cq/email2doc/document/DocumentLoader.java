package com.cq.email2doc.document;

import java.util.List;

import com.cq.udr.Document;

public interface DocumentLoader
{

  public Document insertDocument(
    DocumentSubmission docSubmission,
    String principalUserName);
  

  public void insertPLRecord(
    List<PublicLawData> pensData);
  
}

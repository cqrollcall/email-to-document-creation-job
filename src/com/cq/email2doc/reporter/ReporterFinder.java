package com.cq.email2doc.reporter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cq.email2doc.Util;
import com.cq.format.IFormat;
import com.cq.service.udr.UdrService;
import com.cq.udr.Reporter;

public class ReporterFinder
{
  private static Log log = LogFactory.getLog(ReporterFinder.class);
  
  @Resource
  private UdrService udrService;
  
  @Resource
  private IFormat<Reporter> reporterFormat;
  
  
  private Map<Long,Reporter> reporterMap = null;
  private Map<String,Long> aliasToReporterIdMap = null;
  private Map<String,Long> bylineToReporterIdMap = null;
  
  
  
  public void init()
  {
    Thread thread = new Thread( new Initializer( this ) );
    thread.setName("ReporterFinder-Initializer");
    thread.setDaemon( true );
    thread.start();
  }
  
  public String convertToBylineString(Reporter reporter)
  {
    if (reporter == null)
      return null;
    return reporterFormat.format( reporter );
  }
  
  public Reporter getReporterByByline(String byline)
  {
    if (byline == null)
      return null;
    
    Reporter reporter = null;
    Long reporterId = bylineToReporterIdMap.get( byline );
    
    if ( reporterId != null )
      reporter = reporterMap.get( reporterId );
    
    return reporter;
  }
  
  
  public String getBylineByEmailAddress( String emailAddress )
  {
    return udrService.getReporterBylineByEmailAddress( emailAddress );
  }
  
  public List<Reporter> findReportersByPartialMatch(String txt)
  {
    if (txt == null || aliasToReporterIdMap == null)
      return null;
    
    txt = txt.toUpperCase();
    
    Set<Long> reportersMatched = new HashSet<Long>();
       
    for (String key: aliasToReporterIdMap.keySet())
    {
      if (key.contains( txt ) )
      {
        reportersMatched.add( aliasToReporterIdMap.get( key ) );
      }
    }
    
    if ( reportersMatched.size() == 0 )
      return null;
    
    List<Reporter> results = new ArrayList<Reporter>();
    for ( Long reporterId: reportersMatched )
    {
      results.add( reporterMap.get( reporterId ) );
    }   
    
    return results;
  }
  
  
  public String getBylineByAlias( String txt )
  {
    Reporter reporter = findReporterByAlias(txt);
    
    if ( reporter != null)
      return udrService.calculateReporterByline( reporter );
    else
      return txt;
  }

  public Reporter findReporterByAlias( String txt )
  {
    if (txt == null || aliasToReporterIdMap == null)
      return null;
    
    txt = txt.toUpperCase();
    
    Reporter reporter = null;
    Long reporterId = aliasToReporterIdMap.get( txt );
    
    if ( reporterId != null )
      reporter = reporterMap.get( reporterId );
    
    return reporter;
  }

  
  public void refreshData()
  {
    try
    {
      Map<Long,Reporter> tmpMap = new HashMap<Long,Reporter>();
      Map<String,Long> tmpAliasToReporterIdMap = new HashMap<String,Long>();
      Map<String,Long> tmpBylineToReporterIdMap = new HashMap<String,Long>();
      
      List<Reporter> reporters = udrService.getAllReporters();
      
      for (Reporter reporter: reporters)
      {
        tmpMap.put(reporter.getId(), reporter);
        
        tmpAliasToReporterIdMap.put( createFirstInitialLastAlias(reporter), reporter.getId());
        tmpAliasToReporterIdMap.put( createFirstLastAlias(reporter), reporter.getId());
        if ( Util.specified( reporter.getEmailAddress() ) )
        {
          tmpAliasToReporterIdMap.put( createEmailAlias(reporter), reporter.getId());
          
       //   log.info("adding reporter "+reporter.getId()+" for alias: /"+createEmailAlias(reporter)+"/");
          
        }
        tmpBylineToReporterIdMap.put(convertToBylineString(reporter), reporter.getId() );
      }
      
      reporterMap = tmpMap;
      aliasToReporterIdMap = tmpAliasToReporterIdMap;
      bylineToReporterIdMap = tmpBylineToReporterIdMap;
      
      log.info("refreshed reporter cache: "+reporterMap.size() +" reporters");
    }
    catch (Throwable t)
    {
      log.warn("unable to refresh the reporter cache", t);
    }
  }
  
  /**
   * As a separate thread, initialize the reporter name data structures after startup.
   *
   */
  private static class Initializer implements Runnable
  {
    private ReporterFinder reporterFinder;

    public Initializer( ReporterFinder reporterFinder )
    {
      this.reporterFinder = reporterFinder;
    }

    public void run()
    {
      long delay = 30000; // first delay is 30 seconds to allow app to start.
      
      while (true)
      {
        try
        {
          Thread.sleep( delay );  
  
          reporterFinder.refreshData();
          
          delay = 4 * 60 * 60 * 1000;  // Now do a refresh four hours from now.
        }
        catch( InterruptedException e )
        {
          // ignore
        }
      }
    }
  }
  
  
  private String createFirstInitialLastAlias( Reporter reporter )
  {
    StringBuilder sb = new StringBuilder();
    
    if (Util.specified(reporter.getFirstName()))
      sb.append(reporter.getFirstName().charAt(0));
    if (Util.specified(reporter.getLastName()))
      sb.append(reporter.getLastName());
    
    return sb.toString().toUpperCase();
  }
  
  private String createFirstLastAlias( Reporter reporter )
  {
    StringBuilder sb = new StringBuilder();
    
    if (Util.specified(reporter.getFirstName()))
      sb.append(reporter.getFirstName());
    if (Util.specified(reporter.getLastName()))
      sb.append(reporter.getLastName());
    
    return sb.toString().toUpperCase();
  }

  private String createEmailAlias( Reporter reporter )
  {
    return reporter.getEmailAddress().toUpperCase();
  }

}

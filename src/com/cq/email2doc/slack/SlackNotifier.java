package com.cq.email2doc.slack;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cq.Util;
import com.cq.email2doc.document.DocumentSubmission;
import com.cq.email2doc.reporter.ReporterFinder;
import com.cq.service.document.utils.MessagingChannel;
import com.cq.service.document.utils.MessagingConfigManager;
import com.cq.service.document.utils.MessagingNotificationConfig;
import com.cq.slack.SendMessageRequestBuilder;
import com.cq.slack.SlackMessageSender;
import com.cq.udr.Document;
import com.cq.udr.DocumentByline;
import com.cq.udr.DocumentSource;
import com.cq.udr.Reporter;
import com.cq.udr.WorkflowType;

public class SlackNotifier {

	private static Log log = LogFactory.getLog(SlackNotifier.class);

	private String ariesAppRootUrl;

	@Resource
	Properties applicationProperties;

	@Resource
	private SlackMessageSender fnSlackMessageSender;

	@Resource
	ReporterFinder reporterFinder;

	public void notify(Document doc, DocumentSubmission submission) {

		String byline = constructByline(doc);

		if (!Util.specified(byline)) {
			Reporter reporter = reporterFinder.findReporterByAlias(submission.getFromEmailAddress());

			if (reporter != null) {
				byline = reporterFinder.convertToBylineString(reporter);
			}
		}

		if (!Util.specified(byline))
			byline = submission.getFromEmailAddress();

		String publicationName = constructPublicationName(doc);
		String slugOrHeadline = constructSlugOrHeadline(doc);

		try {
			String workflowStatus = constructWorkflowStatus(doc);

			StringBuilder sb = new StringBuilder();

			sb.append("Submitted to ").append(workflowStatus).append(": *").append(slugOrHeadline).append("*:  ").append(" by ").append(byline).append(" for _").append(publicationName).append("_. ");
			sb.append("<" + constructEditDocumentUrl(doc.getId())).append("|").append("Lock and Edit>  ");
			sb.append("<" + constructViewDocumentUrl(doc.getId())).append("|").append("View>");

			String slackMessage = sb.toString();

			MessagingNotificationConfig channels = MessagingConfigManager.configuration.get(doc.getSource().getId());
			
			if(channels != null && channels.getMessagingChannels() != null){				
				for (MessagingChannel channel : channels.getMessagingChannels()) {
					// notifyHipChat( channel, hipChatMessage );
					notifySlack(channel, slackMessage);
				}
			} else {
				log.info("Slack notifier not configured for document source id : " + doc.getSource().getId());
			}
			
		} catch (Throwable t) {
			log.warn("unable to send Slack message", t);
		}

	}

	private String constructEditDocumentUrl(Long documentId) {
		return constructEditDocumentUrl(String.valueOf(documentId));
	}

	private String constructEditDocumentUrl(String documentId) {
		return getAriesAppRootUrl() + "/edit?doc=" + String.valueOf(documentId);
	}

	private String constructViewDocumentUrl(Long documentId) {
		return constructViewDocumentUrl(String.valueOf(documentId));
	}

	private String constructViewDocumentUrl(String documentId) {
		return getAriesAppRootUrl() + "/view?doc=" + String.valueOf(documentId);
	}

	private String constructWorkflowStatus(Document doc) {
		String workflowStatus = null;

		if (doc.getWorkflowType().getId() != null) {
			try {
				workflowStatus = WorkflowType.Enum.findById(doc.getWorkflowType().getId()).name();
			} catch (Throwable t) {
				workflowStatus = "UNKNOWN-Status";
			}
		}

		return workflowStatus;
	}

	private String constructPublicationName(Document doc) {
		if (doc.getSource().getId() == DocumentSource.BACKGROUNDER) {
			return "Topic Backgrounder";
		} else if (doc.getSource().getId() == DocumentSource.CQLIVE) {
			return "CQ Now";
		} else if (doc.getSource().getId() == DocumentSource.CQACTIONALERTS) {
			return "Action Alerts";
		} else if (doc.getSource().getId() == DocumentSource.CQBUDGET) {
			return "Budget Tracker";
		} else if (doc.getSource().getId() == DocumentSource.CQHEALTH) {
			return "HealthBeat";
		} else if (doc.getSource().getId() == DocumentSource.COMMITTEENEWS) {
			return "Committee News";
		} else if (doc.getSource().getId() == DocumentSource.CQ_LEGAL) {
			return "CQ Legal";
		} else if (doc.getSource().getId() == DocumentSource.CQSENATE) {
			return "Senate Action Reports";
		} else if (doc.getSource().getId() == DocumentSource.CQWEEKLY) {
			return "CQ Magazine";
		} else {
			return "CQ News";
		}
	}

	private String constructSlugOrHeadline(Document doc) {
		if (Util.specified(doc.getSourceFile())) {
			return doc.getSourceFile();
		} else {
			return "\"" + doc.getHeadline() + "\"";
		}
	}

	private String constructByline(Document document) {
		List<String> bylineList = new ArrayList<String>();

		for (DocumentByline byline : document.getDocumentBylines()) {
			try {
				String reporterName = null;
				if (byline.getReporter() != null)
					reporterName = reporterFinder.convertToBylineString(byline.getReporter());
				else
					reporterName = byline.getReporterName();

				bylineList.add(reporterName);
			} catch (org.hibernate.ObjectNotFoundException onfe) {
				log.warn("unable to find Reporter for document: " + document.getId());
			}
		}

		if (bylineList.size() == 0 && Util.specified(document.getByline()))
			return document.getByline().trim();

		return StringUtils.join(bylineList, ", ");
	}

	public String getAriesAppRootUrl() {
		return ariesAppRootUrl;
	}

	public void setAriesAppRootUrl(String ariesAppRootUrl) {
		this.ariesAppRootUrl = ariesAppRootUrl;
	}

	private void notifySlack(MessagingChannel channel, String msg) {
		
		 String fnSlackChannel 
	        = "prod".equalsIgnoreCase(applicationProperties.getProperty("env"))
	            ? channel.getFNSlackChannelName() 
	            : channel.getFNSlackChannelNameForQA();
	            
		try {

			if (fnSlackChannel != null) {
				log.info("Sending Slack notification to #" + fnSlackChannel + ": " + msg);

				fnSlackMessageSender.sendMessage(new SendMessageRequestBuilder().forMessage(msg).from("Aries", "http://jukebox.cq.com/editorial/icons/aries-slack.png").toChannel(fnSlackChannel).build());
			}
		} catch (Throwable t) {
			log.warn("Unable to send notification to Slack for committee event", t);
		}
	}

}

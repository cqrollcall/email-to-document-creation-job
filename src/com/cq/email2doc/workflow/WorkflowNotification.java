package com.cq.email2doc.workflow;


/**
 * Represents a workflow notification parsed from an email or other messaging system.
 * <p>
 * The data has been broken down from the freeform text of an email into logical
 * parts: headline, author, status, link to edit document, etc.
 * 
 */

public class WorkflowNotification
{

  private String publication;
  private String hed;
  private String byline;
  private String editUrl;
  private String status;
  
  
  public String getHed()
  {
    return hed;
  }
  
  public void setHed(
    String hed)
  {
    this.hed = hed;
  }


  public String getEditUrl()
  {
    return editUrl;
  }

  public void setEditUrl(
    String editUrl)
  {
    this.editUrl = editUrl;
  }

  public String getStatus()
  {
    return status;
  }

  public void setStatus(
    String status)
  {
    this.status = status;
  }

  public String getByline()
  {
    return byline;
  }

  public void setByline(
    String byline)
  {
    this.byline = byline;
  }

  public String getPublication()
  {
    return publication;
  }

  public void setPublication(
    String publication)
  {
    this.publication = publication;
  }


}

package com.cq.email2doc.workflow;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cq.email2doc.mail.EmailSubmission;
import com.cq.email2doc.mail.EmailSubmissionPart;

public class WorkflowNotificationParser
{
  private static final Pattern fromMailtoEmailPattern = Pattern.compile("<(.+?)<mailto:(.+?)>>");
  private static final Pattern fromEmailPattern = Pattern.compile("<(.+?)>");

  private static final Pattern subjectForForwardPattern = Pattern.compile("(FW:|FWD:)", Pattern.CASE_INSENSITIVE);
  
  private static final Pattern publicationInSubjectPattern = Pattern.compile("\\[(.+?)\\]");
  
  /**
   * Parses an EmailSubmission into a WorkflowNotification
   */
  public WorkflowNotification parse(
    WorkflowNotificationParsingContext context,
    EmailSubmission submission )
  {
    WorkflowNotification notification = new WorkflowNotification();
    
    //parseSubjectIntoHed( context, trimOrNull( submission.getSubject() ), notification );
    
    notification.setPublication( parsePublication( trimOrNull( submission.getSubject() ) ) );
    
    String content = null;
    EmailSubmissionPart part = submission.findPart("text/plain");
    if (part == null)
      part = submission.findPart("TEXT/PLAIN");

    if (context.isVerbose())
      System.out.println("Starting with part content:\n"+part.getContent());
      
    if ( part.getContentType() == null || part.getContentType().startsWith( "text/plain" ) )
    {
      content = stripNonAsciiContent( part.getContent() );
    }    
        
    List<String> nonBlankLines = new ArrayList<String>();
    
    for (String line: content.split("\n"))
    {
      // In parsing the body we treat each "line" as paragraph.
      // Blank lines are skipped.      
      line = trimOrNull( line );
      
      if (line != null)
        nonBlankLines.add( line );    
    }
    
    
    
    notification.setHed( findContentForHeader( context, nonBlankLines, "Post" ) );
    notification.setEditUrl( findContentForHeader( context, nonBlankLines, "Edit" ) );
    notification.setByline( findContentForHeader( context, nonBlankLines, "Last modify by" ) );
    notification.setStatus( findContentForHeader( context, nonBlankLines, "Post status changed to" ) );
    
    
    
    
    return notification;
  }
  
  
  private String findContentForHeader
    (WorkflowNotificationParsingContext context,
        List<String> nonBlankLines,
     String headerName)
  {
    
    for ( String line: nonBlankLines )
    {
      if ( line.startsWith( headerName ) )
      {
        String content = line.substring(headerName.length());
        if (content.startsWith(":"))
          content = content.substring(1);
        return content.trim();        
      }
    }
    return null;
  }
  
  
  /** 
   * Parse the email subject into a headline, with special handling
   * to strip out reply headers (RE:) and forward headers (FW:)
   * <p>
   * If either a reply or a forward is detected then the context is
   * updated.
   * 
   * @param context
   * @param subject
   * @param notification
   */
  
  private void parseSubjectIntoHed(
    WorkflowNotificationParsingContext context,
    String subject,
    WorkflowNotification notification)
  {
    if ( subject == null )
      return;
    
    // Check for reply:  RE:
    
    if ( subject.matches( ".*RE:.*" ) )
    {
      subject = subject.replaceAll("(?i)RE:", "");
      context.setReply( true );      
    }
    

    // Check for forwards:   FW: or FWD:

    Matcher matcher = subjectForForwardPattern.matcher( subject );
    
    if ( matcher.find() )
    {
      context.setForward( true );      

      subject = subject.replaceAll("(?i)FW:", "");
      subject = subject.replaceAll("(?i)FWD:", "");
    }
    
    subject = trimOrNull( subject );
    
    notification.setHed( subject );
  }
  
  
  /**
   * Parse out the email address from the "from" header.
   * 
   * @param from
   * @return
   */
  private String parsePublication(
    String subject)
  {
    if ( subject == null )
      return null;
    
    // Some possibilities:
    //    [CQ on Defense] I'm texty and I know it pending review  
    
    Matcher matcher1 = publicationInSubjectPattern.matcher( subject );
    
    if (matcher1.find())
    {
      return matcher1.group(1);
    }
    
    return null;
  }
  
  
  /**
   * Return the string trimmed of whitespace, and return null if the string
   * ends up empty.
   * 
   * @param src
   * @return
   */
  private String trimOrNull( 
    String src)
  {
    if (src != null)
    {
      src = src.trim();
      
      if (src.length() == 0)
        src = null;
    }
    
    return src;
  }
  
  private String stripNonAsciiContent(
    String content)
  {
    return content.replaceAll("[^\\x00-\\x7f]", "");
  }  
  
}

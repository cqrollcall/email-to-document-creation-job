package com.cq.email2doc.mail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cq.email2doc.Util;

public class EmailSubmission
{
  private String mailboxName;
  private String mailboxType;
  private String from;
  private String subject;
  private List<EmailSubmissionPart> parts = new ArrayList<EmailSubmissionPart>();
  
  private static Log log = LogFactory.getLog(EmailSubmission.class);
  
  public String getFrom()
  {
    return from;
  }
  public void setFrom(
    String from)
  {
    this.from = from;
  }
  public String getSubject()
  {
    return subject;
  }
  public void setSubject(
    String subject)
  {
    this.subject = subject;
  }
  
  public void addPart(EmailSubmissionPart part)
  {
    parts.add( part );
  }
  
  public void addPart(String content, String contentType)
  {
    EmailSubmissionPart part = new EmailSubmissionPart();
    part.setContent( Util.removeNonPrintableCharacters( content ) );
    part.setContentType( contentType );
    
    parts.add( part );
  }

  public List<EmailSubmissionPart> getParts()
  {
    return parts;
  }
  
  public EmailSubmissionPart findPart(String contentType)
  {
    if (contentType == null)
      return null;
    
    for (EmailSubmissionPart part: parts)
    {
      if (part.getContentType().startsWith(contentType))
        return part;
    }
    
    return null;
  }
  public String getMailboxName()
  {
    return mailboxName;
  }
  public void setMailboxName(
    String mailboxName)
  {
    this.mailboxName = mailboxName;
  }
  public String getMailboxType()
  {
    return mailboxType;
  }
  public void setMailboxType(
    String mailboxType)
  {
    this.mailboxType = mailboxType;
  }


  
}

package com.cq.email2doc.mail;

import java.util.List;


public interface SubmissionFetcher
{
   
  /**
   * Fetch email submissions from a mailbox and delete them as we go.
   * 
   */
  public List<EmailSubmission> fetchSubmissions();
  
  
  /**
   * Return the name of the mailbox used for email submissions
   * @return
   */
  public String getMailboxName();


}

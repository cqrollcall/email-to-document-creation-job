package com.cq.email2doc.mail;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cq.email2doc.Util;

public class SubmissionFetcherImpl implements SubmissionFetcher {
	private static Log log = LogFactory.getLog(SubmissionFetcherImpl.class);

	protected static final SimpleDateFormat timestampedDirectoryNameFormatter = new SimpleDateFormat("yyyy-MM/dd");
	private SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");

	private static final List<EmailSubmission> emptySubmissionsList = Collections.unmodifiableList(new ArrayList<EmailSubmission>());

	private String usernameAtDomain;
	private String password;
	private String mailboxType;

	@Resource
	Properties applicationProperties;

	/**
	 * Return the name of the mailbox used for email submissions
	 * 
	 * @return
	 */
	@Override
	public String getMailboxName() {
		return usernameAtDomain;
	}

	/**
	 * 
	 */
	public List<EmailSubmission> fetchSubmissions() {
		List<EmailSubmission> result = new ArrayList<EmailSubmission>();

		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		try {
      
      if ( usernameAtDomain.startsWith( "DISABLED" ) )
      {
        log.info( "check for " + usernameAtDomain + " disabled." );
        return result;
      }
      
      log.info( "check for " + usernameAtDomain + " mailboxType=" + mailboxType );
      
			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore("imaps");

			if (!store.isConnected())
				store.connect("imap.gmail.com", usernameAtDomain, password);

			Folder inbox = store.getFolder("INBOX");

			inbox.open(Folder.READ_WRITE);

			Message[] messages = inbox.getMessages();

			if (log.isDebugEnabled())
				log.debug("fetchSubmissions: messages[].length: " + (messages == null ? "NULL" : String.valueOf(messages.length)));

			for (Message m : messages) {
				/*
				 * try { m.writeTo(System.out); } catch (IOException ioe) {
				 * log.warn("unable to get raw input stream of message", ioe); }
				 */

				EmailSubmission submission = null;
				try {
					submission = new EmailSubmission();
					submission.setMailboxName(getMailboxName());
					submission.setMailboxType(getMailboxType());
					submission.setFrom(getFromAddress(m));
					submission.setSubject(getSubject(m));

					extractContentIntoSubmission(m, submission);
				} catch (IOException ioe) {
					log.warn("unable to parse message", ioe);
				}

				if (submission != null) {
					result.add(submission);

					logSubmission(submission);
				}
				m.setFlag(Flag.DELETED, true);
			}

			inbox.close(true); // close the box with EXPUNGE=true to really
								// delete the messages.

			return result;

		} catch (javax.mail.AuthenticationFailedException afe) {
			log.warn("unable to authenticate to IMAP for username/password: " + usernameAtDomain + "/" + password);
		} catch (NoSuchProviderException e) {
			log.warn("unable to establish IMAP-over-SSL session", e);
		} catch (MessagingException e) {
			log.warn("unable to get messages", e);
		}

		return null;
	}

	/**
	 * Record a copy of email submissions that we attempt to process. We can use
	 * this store of files as source for unit tests in the future when adding
	 * new features.
	 * 
	 * @param submission
	 */
	private void logSubmission(EmailSubmission submission) {
		Date now = new Date();
		StringBuilder sb = new StringBuilder();

		File submissionsDir = new File(getEmailSubmissionDirectory(), timestampedDirectoryNameFormatter.format(now));
		if (!(submissionsDir.exists()))
			submissionsDir.mkdirs();

		File file = new File(submissionsDir, timestampFormat.format(now) + ".txt");

		try {
			sb.append("%%%%From: ").append(submission.getFrom()).append("\n");
			sb.append("%%%%Subject: ").append((submission.getSubject() == null ? "" : submission.getSubject())).append("\n");

			for (EmailSubmissionPart part : submission.getParts())
				sb.append("%%%%ContentPart[" + part.getContentType() + "]:\n").append(part.getContent()).append("\n");

			FileUtils.writeStringToFile(file, sb.toString(), true);
		} catch (IOException e) {
			log.warn("unable to write email submission: " + file.getAbsolutePath(), e);
		}

	}

	private String getFromAddress(Message m) throws MessagingException {
		for (Address a : m.getFrom()) {
			
			// A little hack to read actual user who sent the content in case if the email is forwarded from cqnews@knowlegis.org
			if(isEmailForwardedFromUnexpectedEmailId(a.toString()) && m.getReplyTo() != null && m.getReplyTo().length > 0){
				return m.getReplyTo()[0].toString();
			}
			
			return a.toString();
		}
		return null;
	}

	private String getSubject(Message m) throws MessagingException, IOException {
		if (m == null || m.getSubject() == null)
			return null;

		String subject = Util.numericEntitiesToPlainText(m.getSubject());

		/*
		 * StringBuilder sb = new StringBuilder(); String txt = subject;
		 * sb.append("SUBJECT characters:\n"); for (int i=0; i< txt.length();
		 * i++) { char c = txt.charAt(i); int cVal = (int)c; sb.append( c
		 * ).append(", ").append(String.valueOf(cVal)).append("\n"); }
		 * log.info(sb.toString());
		 */

		return (subject.replaceAll("(?i)RE:", "")).replaceAll("(?i)FW:", "");
	}

	private void extractContentIntoSubmission(Message m, EmailSubmission submission) throws MessagingException, IOException {
		log.info("content type: " + m.getContentType());
		log.info("content.class: " + m.getContent().getClass().getCanonicalName());

		if (m.getContent() instanceof String) {
			log.info("processing content as String");
			extractPartContentIntoSubmission(m, 0, submission);
		} else if (m.getContent() instanceof Multipart) {
			Multipart multipart = (Multipart) m.getContent();
			log.info("processing content as Multipart: " + multipart.getCount() + " parts");

			for (int i = 0; i < multipart.getCount(); i++) {
				BodyPart bodyPart = multipart.getBodyPart(i);
				extractPartContentIntoSubmission(bodyPart, i, submission);
			}
		}
	}

	/**
	 * Extract the text content and the content type from the email part into
	 * the email submission object
	 * 
	 * @param part
	 * @param submission
	 * @throws MessagingException
	 * @throws IOException
	 */
	private void extractPartContentIntoSubmission(Part part, int partIndex, EmailSubmission submission) throws MessagingException, IOException {
		log.info("extractPartContentIntoSubmission: processing part " + partIndex + " content type: " + part.getContentType() + " subject=" + submission.getSubject() + " from=" + submission.getFrom());

		if (part.getContentType().startsWith("text/plain;") || part.getContentType().startsWith("TEXT/PLAIN;")) {
			submission.addPart((String) part.getContent(), part.getContentType());
		} else if (part.getContentType().startsWith("text/html;") || part.getContentType().startsWith("TEXT/HTML;")) {
			submission.addPart((String) part.getContent(), part.getContentType());
		} else if (part.getContentType().toLowerCase().startsWith("multipart/alternative")) {
			if (part.getContent() instanceof Multipart) {
				Multipart mp = (Multipart) part.getContent();
				for (int i = 0; i < mp.getCount(); i++) {
					BodyPart bp = mp.getBodyPart(i);
					if (bp != null) {
						Object oContent = bp.getContent();
						if (oContent != null) {
							if (oContent instanceof java.lang.String) {
								submission.addPart((String) bp.getContent(), bp.getContentType());
							} else {
								log.warn("part " + i + " is unhandled type " + oContent.getClass().getName());
							}
						}
					}
				}
			}
		} else {
			log.warn("Unable to email submission fully, skipping part with content type: " + part.getContentType());
		}

	}

	/**
	 * 
	 */
	public void listTopLevelFolders() {
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		try {
			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore("imaps");

			if (!store.isConnected())
				store.connect("imap.gmail.com", usernameAtDomain, password);

			Folder rootFolder = store.getDefaultFolder();

			System.out.println("rootFolder.fullName: " + rootFolder.getFullName());
			System.out.println("rootFolder.name: " + rootFolder.getName());

			Folder[] folders = rootFolder.list();

			System.out.println("rootFolder.folders[].length: " + (folders == null ? "NULL" : String.valueOf(folders.length)));

			int i = 0;
			for (Folder folder : folders) {
				System.out.println("folder[" + i + "].fullName: " + folder.getFullName());
				System.out.println("folder[" + i + "].name: " + folder.getName());
			}

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.exit(2);
		}

	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsernameAtDomain() {
		return usernameAtDomain;
	}

	public void setUsernameAtDomain(String usernameAtDomain) {
		this.usernameAtDomain = usernameAtDomain;
	}

	public String getMailboxType() {
		return mailboxType;
	}

	public void setMailboxType(String mailboxType) {
		this.mailboxType = mailboxType;
	}

	public String getEmailSubmissionDirectory() {
		return applicationProperties.getProperty("email-submission-dir");
	}
	
	public String[] getEmailForwarderAddressList() {
		String list = applicationProperties.getProperty("email-submission:consider-reply-to-list");
		if(list != null && list.trim() != ""){
			return list.split(",");
		}
		return new String[] {};
	}
	
	public boolean isEmailForwardedFromUnexpectedEmailId(String emailId){
		String[] mailIds = getEmailForwarderAddressList();
		if(emailId != null && mailIds.length > 0){
			for (int i = 0; i < mailIds.length; i++) {
				if(emailId.toUpperCase().contains(mailIds[i].toUpperCase())){
					return true;
				}
			}
		}
		return false;
	}
}

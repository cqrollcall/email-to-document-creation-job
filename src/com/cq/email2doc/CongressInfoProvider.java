package com.cq.email2doc;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.cq.udr.Congress;

public class CongressInfoProvider {
	private static Log log = LogFactory.getLog(CongressInfoProvider.class);

	@Resource
	private HibernateTemplate udrHibernateTemplate;

	@Resource
	Properties applicationProperties;

	public int getCurrentCongress() {
		return Integer.parseInt(applicationProperties.getProperty("current-congress"));
	}

	public List<Congress> getCongressForDate(Date date) {
		try {
			return (List<com.cq.udr.Congress>) udrHibernateTemplate.find("from Congress where startDate <= ? and endDate >=?", date, date);

		} catch (Throwable t) {
			log.warn("unable to determine congress", t);
		}
		return null;
	}

}
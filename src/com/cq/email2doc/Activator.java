package com.cq.email2doc;

import java.io.File;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Activator implements BundleActivator
{
	  private static Log log = LogFactory.getLog( Activator.class );
	  
	  private BundleContext bundleContext;
	  private File contextFile;
	  private AbstractXmlApplicationContext context;
	  private Properties properties = new Properties();

	  public void setProperties( Properties properties )
	  {
	    this.properties = properties;
	  }
	  
	  
	  /**
	   * 
	   * for unit tests
	   */
	  public void setContextFile( File contextFile )
	  {
	    this.contextFile = contextFile;
	  }
	  
	  /**
	   * 
	   * for unit tests
	   */
	  public BeanFactory getBeanFactory()
	  {
	    return context;
	  }
	  
	  public void start()
	  {
	    log.info( "Starting Email to Document Creation Job" );
	    
	    if( contextFile == null )
	    {
	      context = new ClassPathXmlApplicationContext();
	      context.setConfigLocation("/META-INF/spring/beans.xml");
	    }
	    else
	    {
	      context = new FileSystemXmlApplicationContext();
	      context.setConfigLocation(contextFile.getPath());
	    }
	    
	    PropertyPlaceholderConfigurer cfg = new PropertyPlaceholderConfigurer();
	    cfg.setProperties( properties );
	    context.addBeanFactoryPostProcessor( cfg );
	    context.addBeanFactoryPostProcessor( new BeanFactoryPostProcessor(){

	      @Override
	      public void postProcessBeanFactory( ConfigurableListableBeanFactory beanFactory ) throws BeansException
	      {
	        beanFactory.registerSingleton( "applicationProperties", properties );
	      }
	      
	    });
	    context.refresh();
	    context.start();
	  }
	  
	  public void stop()
	  {
	    log.info( "Stopping Email to Document Creation Job" );

	    if( context != null )
	    {
	      try
	      {
	        context.close();
	      }
	      catch( Exception e )
	      {
	        log.error( e.getMessage() );
	      }
	    }
	    
	    synchronized( this )
	    {
	      this.notifyAll();
	    }
	  }


	  @Override
	  public void start( BundleContext context ) throws Exception
	  {
	    this.bundleContext = context;
	    start();
	  }


	  @Override
	  public void stop( BundleContext context ) throws Exception
	  {
	    stop();
	  }
}
